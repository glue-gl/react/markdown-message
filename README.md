# MarkdownMessage

React component that allows to add bold with internationalization

## Usage

To install it:

> npm install --save @gluedigital/markdown-message

To use it:

```javascript
import MarkdownMessage from '@gluedigital/markdown-message'

// ...

<MarkdownMessage id="id.message" />
```
## Warning

<IntlProvider> needs to exist in the component ancestry.

## Options

The following props can be used:

| Name | Type | Description |
|------|------|-------------|
| id | string | Text that you want to internationalizate |
| values | object | You can use this like FormattedMessage |

## Developing

This package uses [nwb](https://github.com/insin/nwb) for the build. Take a look at their documentation for more info.

TL;DR: after installing nwb, just do `npm start` to open the dev environment.
