import type { Props } from 'react-intl/src/components/message';
import { ReactNode } from 'react';
type FormattedMessageProps = Props<Record<string, ReactNode>> & {
    id: string;
};
export default function MarkdownMessage(props: FormattedMessageProps): import("react/jsx-runtime").JSX.Element;
export {};
