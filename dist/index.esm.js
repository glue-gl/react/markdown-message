// src/markdown.tsx
import { FormattedMessage } from "react-intl";
import { jsx } from "react/jsx-runtime";
function MarkdownMessage(props) {
  return /* @__PURE__ */ jsx(FormattedMessage, { ...props, children: (text) => /* @__PURE__ */ jsx("span", { children: (Array.isArray(text) ? text[0] : text).toString().split("*").map((bit, i) => i % 2 ? /* @__PURE__ */ jsx("strong", { children: bit }, i) : bit) }) });
}
export {
  MarkdownMessage as default
};
//# sourceMappingURL=index.esm.js.map
