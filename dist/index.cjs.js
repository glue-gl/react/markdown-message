"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// src/index.ts
var index_exports = {};
__export(index_exports, {
  default: () => MarkdownMessage
});
module.exports = __toCommonJS(index_exports);

// src/markdown.tsx
var import_react_intl = require("react-intl");
var import_jsx_runtime = require("react/jsx-runtime");
function MarkdownMessage(props) {
  return /* @__PURE__ */ (0, import_jsx_runtime.jsx)(import_react_intl.FormattedMessage, { ...props, children: (text) => /* @__PURE__ */ (0, import_jsx_runtime.jsx)("span", { children: (Array.isArray(text) ? text[0] : text).toString().split("*").map((bit, i) => i % 2 ? /* @__PURE__ */ (0, import_jsx_runtime.jsx)("strong", { children: bit }, i) : bit) }) });
}
//# sourceMappingURL=index.cjs.js.map
