import React from 'react'
import { render } from '@testing-library/react'
import Component from '../demo/src/index'

describe('Component', () => {
  it('displays the message', async () => {
    const { findByText } = render(<Component />)

    await findByText((_, element) => {
      const text = element?.textContent
      return text === 'This is a test with bold text.'
    })
  })
})
