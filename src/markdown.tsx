import { FormattedMessage } from 'react-intl'
import type { Props } from 'react-intl/src/components/message'
import { ReactNode } from 'react'

type FormattedMessageProps = Props<Record<string, ReactNode>> & {
  id: string
}

export default function MarkdownMessage(props: FormattedMessageProps) {
  return (
    <FormattedMessage {...props}>
      {(text) => (
        <span>
          {(Array.isArray(text) ? text[0] : text)!
            .toString()
            .split('*')
            .map((bit, i) => (i % 2 ? <strong key={i}>{bit}</strong> : bit))}
        </span>
      )}
    </FormattedMessage>
  )
}
