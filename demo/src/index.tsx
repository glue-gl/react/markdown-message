import { IntlProvider } from 'react-intl'
import MarkdownMessage from '../../src/markdown'

export default function Demo() {
  const messages = { test: 'This is a *test* with *bold* text.' }
  return (
    <div>
      <h1>MarkdownMessage Demo</h1>
      <IntlProvider locale="en" messages={messages}>
        <MarkdownMessage id="test" />
      </IntlProvider>
    </div>
  )
}
